<?php

namespace Drupal\Tests\http_status_code_test\FunctionalJavascript;

use Drupal\FunctionalJavascriptTests\WebDriverTestBase;

/**
 * Tests Javascript on the global settings form.
 *
 * @group http_status_code_test
 */
class SettingsFormTest extends WebDriverTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Modules to install.
   *
   * @var array
   */
  protected static $modules = [
    'http_status_code_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Create an admin user.
    $admin_user = $this
      ->drupalCreateUser([
        'access administration pages',
        'configure http status code test config',
      ]);
    $this->drupalLogin($admin_user);
  }

  /**
   * Test global settings form.
   */
  public function testSettingsForm() {
    $assert_session = $this->assertSession();
    $page = $this->getSession()->getPage();

    $this->drupalGet('/admin/config/development/http-status-code-test');

    $assert_session->pageTextContains('Access the endpoint');

    $value = $page
      ->findField('endpoint_path')
      ->getValue();

    $this->assertSame($value, '/http-status-code-test');

    $host = $this->container->get('request_stack')->getCurrentRequest()->getSchemeAndHttpHost();
    $assert_session->pageTextContains("Access the endpoint at $host/http-status-code-test?code=[HTTP Status Code]");
    $assert_session->pageTextContains("E.g. $host/http-status-code-test?code=503");

    $page
      ->findField('endpoint_path')
      ->setValue('/new-endpoint-path');

    $javascript = <<<JS
(function(){
  document.getElementById('edit-endpoint-path').blur();
})()
JS;
    $this->getSession()
      ->evaluateScript($javascript);

    $assert_session->pageTextContains("Access the endpoint at $host/new-endpoint-path?code=[HTTP Status Code]");
    $assert_session->pageTextContains("E.g. $host/new-endpoint-path?code=503");
  }

}
