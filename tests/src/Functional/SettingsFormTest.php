<?php

namespace Drupal\Tests\http_status_code_test\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests the global settings form.
 *
 * @group http_status_code_test
 */
class SettingsFormTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * The test administrative user.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * The test non-administrative user.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $nonAdminUser;

  /**
   * Modules to install.
   *
   * @var array
   */
  protected static $modules = [
    'http_status_code_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Create an admin user.
    $this->adminUser = $this
      ->drupalCreateUser([
        'access administration pages',
        'configure http status code test config',
      ]);
    // Create a non-admin user.
    $this->nonAdminUser = $this
      ->drupalCreateUser([
        'access administration pages',
      ]);
  }

  /**
   * Tests route permissions.
   */
  public function testRoutePermissions() {
    $assert_session = $this->assertSession();

    $this->drupalLogin($this->nonAdminUser);
    // Non-admin user is unable to access settings page.
    $this->drupalGet('/admin/config/development/http-status-code-test');
    $assert_session->statusCodeEquals(403);

    $this->drupalLogin($this->adminUser);
    // Admin user is unable to access settings page.
    $this->drupalGet('/admin/config/development/http-status-code-test');
    $assert_session->statusCodeEquals(200);
  }

  /**
   * Tests the form.
   */
  public function testForm() {
    $assert_session = $this->assertSession();
    $page = $this->getSession()->getPage();

    $this->drupalLogin($this->adminUser);
    $this->drupalGet('/admin/config/development/http-status-code-test');

    $page
      ->findField('endpoint_path')
      ->setValue('invalid-path');

    $page->pressButton('Save configuration');

    $assert_session->pageTextContains('The alias path has to start with a slash.');
  }

}
