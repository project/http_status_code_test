<?php

namespace Drupal\Tests\http_status_code_test\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests endpoint.
 *
 * @group http_status_code_test
 */
class EndpointTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * The test administrative user.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * The test non-administrative user.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $nonAdminUser;

  /**
   * Modules to install.
   *
   * @var array
   */
  protected static $modules = [
    'http_status_code_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Create an admin user.
    $this->adminUser = $this
      ->drupalCreateUser([
        'access administration pages',
        'configure http status code test config',
      ]);
    // Create a non-admin user.
    $this->nonAdminUser = $this
      ->drupalCreateUser([
        'access content',
      ]);
  }

  /**
   * Tests route permissions.
   */
  public function testEndpoint() {
    $assert_session = $this->assertSession();
    $page = $this->getSession()->getPage();

    $this->drupalLogin($this->adminUser);
    $this->drupalGet('/admin/config/development/http-status-code-test');

    $page
      ->findField('enabled')
      ->check();
    $page->pressButton('Save configuration');

    $this->drupalLogin($this->nonAdminUser);

    // Test various responses.
    $this->drupalGet('/http-status-code-test');
    $assert_session->statusCodeEquals(200);
    $assert_session->pageTextContains('No HTTP status code provided.');

    $this->drupalGet('/http-status-code-test', ['query' => ['code' => 503]]);
    $assert_session->statusCodeEquals(503);
    $assert_session->pageTextContains('HTTP Status Code: 503 (Service Unavailable)');

    $this->drupalGet('/http-status-code-test', ['query' => ['code' => 600]]);
    $assert_session->statusCodeEquals(200);
    $assert_session->pageTextContains('Invalid HTTP status code provided');

    $this->drupalGet('/http-status-code-test', ['query' => ['code' => '400dfd']]);
    $assert_session->statusCodeEquals(200);
    $assert_session->pageTextContains('Invalid HTTP status code provided');

    // Update the endpoint path config and verify the dynamic route is updated.
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('/admin/config/development/http-status-code-test');

    $page
      ->findField('endpoint_path')
      ->setValue('/new-endpoint-path');
    $page->pressButton('Save configuration');

    $this->drupalLogin($this->nonAdminUser);

    $this->drupalGet('/http-status-code-test');
    $assert_session->statusCodeEquals(404);

    $this->drupalGet('/new-endpoint-path');
    $assert_session->statusCodeEquals(200);
    $assert_session->pageTextContains('No HTTP status code provided.');
  }

}
