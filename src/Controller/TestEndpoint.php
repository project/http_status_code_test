<?php

namespace Drupal\http_status_code_test\Controller;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;

/**
 * Returns Entity Details.
 */
class TestEndpoint implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * The Symfony request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The Drupal config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructor.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The Symfony request stack.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The Drupal route builder service.
   */
  public function __construct(RequestStack $request_stack, ConfigFactoryInterface $config_factory) {
    $this->requestStack = $request_stack;
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack'),
      $container->get('config.factory')
    );
  }

  /**
   * Function to print the entity details.
   */
  public function content(Request $request) {
    $requested_status_code = $request->query->get('code');

    if (empty($requested_status_code)) {
      $content = $this->t('No HTTP status code provided. Send a request with the status code as the value of the "code" query string parameter. E.g. @host@path?code=503', [
        '@host' => $this->requestStack->getCurrentRequest()->getSchemeAndHttpHost(),
        '@path' => $this->configFactory->get('http_status_code_test.settings')->get('endpoint_path'),
      ]);
      $status_code = 200;
    }
    elseif ($requested_status_code < 100 || $requested_status_code >= 600 || !ctype_digit($requested_status_code)) {
      $content = $this->t('Invalid HTTP status code provided: @code. A 200 status code has been returned instead.', [
        '@code' => $requested_status_code,
      ]);
      $status_code = 200;
    }
    else {
      $content = $this->t('HTTP Status Code: @code (@text)', [
        '@code' => $requested_status_code,
        '@text' => Response::$statusTexts[$requested_status_code] ?? 'Other',
      ]);
      $status_code = $requested_status_code;
    }

    return new Response(
      $content,
      $status_code,
      ['content-type' => 'text/html']
    );
  }

}
