<?php

namespace Drupal\http_status_code_test\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteBuilderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Defines a form that configures your_module’s settings.
 */
class ConfigForm extends ConfigFormBase {

  /**
   * The Symfony request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The Drupal route builder service.
   *
   * @var \Drupal\Core\Routing\RouteBuilderInterface
   */
  protected $routeBuilder;

  /**
   * Constructor.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The Symfony request stack.
   * @param \Drupal\Core\Routing\RouteBuilderInterface $route_builder
   *   The Drupal route builder service.
   */
  public function __construct(RequestStack $request_stack, RouteBuilderInterface $route_builder) {
    $this->requestStack = $request_stack;
    $this->routeBuilder = $route_builder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack'),
      $container->get('router.builder')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'http_status_code_test_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'http_status_code_test.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $message = $this->t('The test endpoint should be disabled when not in active use.');
    $this->messenger()->addWarning($message);

    $form['enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable'),
      '#description' => $this->t('Whether or not the endpoint is enabled'),
      '#config_target' => 'http_status_code_test.settings:enabled',
    ];

    $form['endpoint_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Endpoint Path'),
      '#description' => $this->t('The path to register for the test endpoint'),
      '#config_target' => 'http_status_code_test.settings:endpoint_path',
      '#required' => TRUE,
    ];

    $form['instruction'] = [
      '#markup' => $this->t('<p>Access the endpoint at <code>@host<span class="dynamic-endpoint-path">@endpoint_path</span>?code=[HTTP Status Code]</code>.</p><p>E.g. <code>@host<span class="dynamic-endpoint-path">@endpoint_path</span>?code=503</code></p>', [
        '@host' => $this->requestStack->getCurrentRequest()->getSchemeAndHttpHost(),
        '@endpoint_path' => $this->config('http_status_code_test.settings')->get('endpoint_path'),
      ]),
    ];

    $form['#attached']['library'][] = 'http_status_code_test/settings-form';

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->routeBuilder->setRebuildNeeded();

    parent::submitForm($form, $form_state);
  }

}
