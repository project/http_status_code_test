<?php

namespace Drupal\http_status_code_test\Routing;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * Generates route based on config.
 */
class RouteGenerator implements ContainerInjectionInterface {

  /**
   * The Drupal config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The Drupal route builder service.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory')
    );
  }

  /**
   * Dynamically register test endpoint route.
   *
   * @return \Symfony\Component\Routing\RouteCollection
   *   A route collection.
   */
  public function routes() {
    $collection = new RouteCollection();
    $config = $this->configFactory->get('http_status_code_test.settings');

    if ($config->get('enabled')) {
      $collection->add("http_status_code_test.test_endpoint", new Route(
        $config->get('endpoint_path'),
        [
          '_title' => 'HTTP Status Code Test Endpoint',
          '_controller' => '\Drupal\http_status_code_test\Controller\TestEndpoint::content',
        ],
        [
          '_access' => 'TRUE',
        ],
        [
          'no_cache' => TRUE,
        ]
      ));
    }

    return $collection;
  }

}
