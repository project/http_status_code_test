Drupal.behaviors.httpStatusCodeTestSettingsForm = {
  attach(context, settings) {
    const dynamicSpan = document.getElementsByClassName(
      'dynamic-endpoint-path',
    );
    once('dynamicEndPointMarkup', '#edit-endpoint-path', context).forEach(
      function (element) {
        element.onchange = function () {
          Array.from(dynamicSpan).forEach(function (innerElement) {
            innerElement.innerHTML = element.value;
          });
        };
      },
    );
  },
};
