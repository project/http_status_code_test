# HTTP Status Code Test

This module registers a test endpoint that returns the HTTP status code that is
passed in the request as a query string parameter. For example,
https://example.com/http-status-code-test?code=503 will return a response with
a 503 status code.

The HTTP Status Code Test module is helpful in situations where Drupal needs to
return a specific status code for testing downstream services, such as Akamai
or Cloudflare. For example, a CDN provider can be configured to intercept
responses with certain status codes to serve a custom error page. With this
module, a developer can test that custom error page by purposefully
triggering it.

## Requirements

This module requires no modules outside of Drupal core.

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

This module provides two configuration options:

- Enabled: Whether or not the endpoint is enabled
  - Default value: `false`
- Endpoint Path: The path to register for the test endpoint
  - Default value: `/http-status-code-test`

**The test endpoint should be disabled when not in active use.**

## Maintainers

- Chris Burge - [chris burge](https://www.drupal.org/u/chris-burge).
